import 'package:flutter/material.dart';
import 'guibuilder.dart';
import 'styles.dart';

class BlogView extends StatelessWidget {
  String _appTitle;
  String _appImage;

  String _title;
  String _author;
  String _article;

  BlogView(
      this._appTitle, this._appImage, this._title, this._author, this._article);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: bgButter,
        child: CustomScrollView(
          slivers: <Widget>[
            sliverAppBar(
                title: _title, imgPath: _appImage, articletitle: _appTitle),
            SliverList(
              delegate: SliverChildBuilderDelegate(
                  (context, index) => ListTile(
                        title: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width * 0.9,
                              child: Text(
                                _title,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 25.0),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 10.0),
                              width: MediaQuery.of(context).size.width * 0.9,
                              child: Text(_author,
                                  style: TextStyle(
                                      fontStyle: FontStyle.italic,
                                      fontSize: 15.0)),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 10.0),
                              width: MediaQuery.of(context).size.width * 0.9,
                              child: Text(_article,
                                  style: TextStyle(
                                      fontStyle: FontStyle.normal,
                                      fontSize: 20.0,
                                      wordSpacing: 1.5,
                                      height: 1.25)),
                            )
                          ],
                        ),
                      ),
                  childCount: 1),
            )
          ],
        ),
      ),
    );
  }
}
