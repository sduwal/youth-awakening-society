import 'package:flutter/material.dart';

final accentRed = Color.fromARGB(255, 200, 16, 16);
final bgBlue = Color.fromARGB(255, 0, 38, 51);
final bgButter = Color.fromARGB(255, 253, 246, 227);

final defaultStyle = TextStyle(
  fontWeight: FontWeight.normal,
  color: Colors.black,
  height: 1.5,
  fontSize: 16
);

final titleStyle = TextStyle(
  color: Colors.black,
  fontWeight: FontWeight.bold,
  fontSize: 24,
  height: 1.5,
);

final cardTitle = TextStyle(
  color: Colors.white,
  fontSize: 18,
  height: 1.2,
);

final cardText = TextStyle(
  color: Colors.white,
  fontSize: 12,
  height: 1.2,
);

final labelStyle = TextStyle(
  color: Colors.black.withOpacity(0.5),
  fontWeight: FontWeight.bold,
  fontSize: 16,
  height: 1.5,
);

final whiteTitleStyle = TextStyle(
  fontWeight: FontWeight.bold,
  color: Colors.white,
  fontSize: 24,
  height: 1.5,
);

final Icon arrowDown = Icon(
  Icons.arrow_drop_down,
  size: 30.0,
  color: Colors.white,
);

final Icon arrowUp = Icon(
  Icons.arrow_drop_up,
  size: 30.0,
  color: Colors.white,
);