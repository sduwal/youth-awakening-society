import 'package:flutter/material.dart';
import 'home.dart';
import 'events.dart';
import 'gallery.dart';
import 'blog.dart';
import 'services.dart';
import 'about.dart';
import 'contact.dart';
import 'styles.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'Youth Awakening Society',
      theme: ThemeData(
        primaryColor: bgBlue,
      ),
      home: Home(),
      routes: {
        '/home': (context) => Home(),
        '/events': (context) => Events(),
        '/gallery': (context) => Gallery(),
        '/blog': (context) => Blog(),
        '/services': (context) => Services(),
        '/about us': (context) => About(),
        '/contact': (context) => Contact(),
      },
    );
  }
}
