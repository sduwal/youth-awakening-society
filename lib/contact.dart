import 'package:flutter/material.dart';
import 'package:youth_awakening_society/content.dart';
import 'package:youth_awakening_society/guibuilder.dart';
import 'package:youth_awakening_society/styles.dart';
import 'dart:convert';
import 'guibuilder.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactState extends State {
  final String title = 'Contact';
  ContactContent content;
  Widget _childContent = Center(child: Text('Loading...'));

  @override
  Widget build(BuildContext context) {
    loadContent(context);
    return buildScreen(context, _childContent);
  }

  Future loadContent(BuildContext context) async {
    String jsonContent = await loadAsset('assets/data/contact.json');
    Map<String, dynamic> decoded = await jsonDecode(jsonContent);
    var programs = <Service>[];
    setState(() {
      content = ContactContent(
        decoded['address'],
        decoded['email'],
        decoded['phone'],
        decoded['hours']
      );
      var widgets = <Widget>[
        buildText("Address", titleStyle),
        buildText(content.address, defaultStyle),
        map(),
        buildText("Email", titleStyle),
        buildText(content.email, defaultStyle),
        buildText("Phone", titleStyle),
        buildText(content.phone, defaultStyle),
        buildText("Office Hours", titleStyle),
      ];
      for (var hour in content.hours) {
        widgets.add(buildText(hour['day'], labelStyle));
        widgets.add(buildText(hour['time'], defaultStyle));
      }
      _childContent = buildSliverList(
        title, 'assets/images/contact.png', 
        widgets
      );
    });
  }

  GestureDetector map() {
    return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: _launchMapsUrl,
        child: Container(
        padding: EdgeInsets.only(top: 20, left: 20, right: 20),
        child: Image.asset("assets/images/map.jpg"),
        ),
    );
  }

  _launchMapsUrl() async {
    final url = 'https://www.google.com/maps/place/Youth+Awakening+Society/@27.6747022,85.4273424,17z/data=!3m1!4b1!4m5!3m4!1s0x39eb1ab17222bc45:0x9ac6b560b7ae7944!8m2!3d27.6746975!4d85.4295311';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}

class Contact extends StatefulWidget {
  @override
  ContactState createState() => ContactState();
}