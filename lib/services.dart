import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:youth_awakening_society/content.dart';
import 'package:youth_awakening_society/guibuilder.dart';
import 'package:youth_awakening_society/styles.dart';

class ServiceState extends State {
  final String title = 'Services';
  ServicesContent content;
  Widget _childContent = Center(child: Text('Loading...'));

  @override
  Widget build(BuildContext context) {
    loadContent(context);
    return buildScreen(context, _childContent);
  }

  Future loadContent(BuildContext context) async {
    String jsonContent = await loadAsset('assets/data/services.json');
    Map<String, dynamic> decoded = await jsonDecode(jsonContent);
    var programs = <Service>[];
    setState(() {
      for (Map<String, dynamic> program in decoded['list']) {
        programs.add(Service(program['title'], program['desc'], program['img']));
      }
      content = ServicesContent(
        decoded['title'],
        decoded['desc'],
        programs
      );
      _childContent = buildContent(context, content);     
    });
  }

  Container buildContent(BuildContext context, ServicesContent content) {
    var progs = <Widget>[];
    for (Service program in content.progs) {
      progs.add(Container(
        padding: EdgeInsets.all(10.0),
        child: buildProgramCard(context, program.title, program.desc, AssetImage('assets/images/' + program.img))
      ));
    }
    var programs;
    if (MediaQuery.of(context).orientation == Orientation.portrait){
      programs = Column(
        children: progs,
      );
      return buildSliverList(title, 'assets/images/services.jpg', <Widget>[
        buildText(content.title, titleStyle),
        buildText(content.desc, defaultStyle),
        programs,
      ]);
    } else {
      return buildSliverListWithGrid(title, 'assets/images/blog_main.jpg', <Widget>[
        buildText(content.title, titleStyle),
        buildText(content.desc, defaultStyle),
      ], progs);
    }
  }
}

class Services extends StatefulWidget {
  @override
  ServiceState createState() => ServiceState();
}
