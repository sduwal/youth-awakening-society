import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'blogCard.dart';
import 'guibuilder.dart';
import 'styles.dart';

class Blog extends StatelessWidget {
  Blog({Key key}) : super(key: key);
  final String title = 'Blog';

  @override
  Widget build(BuildContext context) {
    return buildScreen(context, _buildList());
  }

  Widget _buildList() {
    Firestore firestore = Firestore.instance;
    firestore.settings(timestampsInSnapshotsEnabled: true);

    return Container(
      color: bgButter,
      child: CustomScrollView(
        slivers: <Widget>[
          sliverAppBar(title: title, imgPath: 'assets/images/blog_main.jpg'),
          StreamBuilder(
            stream: firestore.collection('blog').snapshots(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.active) {
                return SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (context, index) {
                      return _buildListItem(
                          context,
                          snapshot.data.documents[
                              snapshot.data.documents.length - index - 1]);
                    },
                    childCount: snapshot.data.documents.length,
                  ),
                );
              } else {
                return SliverList(
                    delegate: SliverChildBuilderDelegate(
                  (context, index) {
                    return ListTile(
                      title: Center(
                          child: Text(
                        "Loading....",
                        style: TextStyle(fontSize: 25.0),
                      )),
                    );
                  },
                  childCount: 1,
                ));
              }
            },
          ),
        ],
      ),
    );
  }

  Widget _buildListItem(BuildContext context, DocumentSnapshot documents) {
    String _title = documents['title'];
    String _author = documents['author'];
    String _type = documents['type'];
    String _article = documents['article'];

    return ListTile(
      title: BlogCard(
          _title.replaceAll("\\n", "\n"),
          _author.replaceAll("\\n", "\n"),
          _type.replaceAll("\\n", "\n"),
          _article.replaceAll("\\n", "\n\n")),
    );
  }
}
