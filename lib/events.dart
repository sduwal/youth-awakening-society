import 'package:flutter/material.dart';
import 'eventCard.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'guibuilder.dart';
import 'styles.dart';

class Events extends StatelessWidget {
  final String title = 'Events';

  @override
  Widget build(BuildContext context) {
    return buildScreen(context, _buildList());
  }

  Widget _buildList() {
    Firestore firestore = Firestore.instance;
    firestore.settings(timestampsInSnapshotsEnabled: true);
    return Container(
      color: bgButter,
      child: CustomScrollView(
        slivers: <Widget>[
          sliverAppBar(title: title, imgPath: 'assets/images/events.jpg'),
          StreamBuilder(
            stream: firestore
                .collection('events')
                .orderBy('timestamp', descending: true)
                .snapshots(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.active) {
                return SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (context, index) {
                      return _buildListItem(
                          context, snapshot.data.documents[index]);
                    },
                    childCount: snapshot.data.documents.length,
                  ),
                );
              } else {
                return SliverList(
                    delegate: SliverChildBuilderDelegate(
                  (context, index) {
                    return ListTile(
                      title: Center(
                          child: Text(
                        "Loading....",
                        style: TextStyle(fontSize: 25.0),
                      )),
                    );
                  },
                  childCount: 1,
                ));
              }
            },
          )
        ],
      ),
    );
  }

  Widget _buildListItem(BuildContext context, DocumentSnapshot document) {
    String name = document['name'];
    String cardImage = document['image'];
    String description = document['description'];
    var timeStamp = document['timestamp'];

    return ListTile(
      title: EventCard(name, cardImage, description, timeStamp),
    );
  }
}
