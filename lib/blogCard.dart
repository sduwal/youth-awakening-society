import 'package:flutter/material.dart';
import 'blogView.dart';
import 'styles.dart';

class BlogCard extends StatelessWidget {
  String _title;
  String _author;
  String _type;
  String _article;

  BlogCard(this._title,this._author,this._type,this._article);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: bgBlue,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => BlogView("Blog Article",'assets/images/blog.jpg',_title, _author, _article)),
          );
        },
        child: Container(
          padding: EdgeInsets.all(10.0),
          child: Row(
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(5.0, 5.0, 16.0, 5.0),
                    width: MediaQuery.of(context).size.width * 0.85,
                    child: Text(
                      _title,
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold, fontSize: 25.0),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 16.0, right: 16.0, left: 5.0),
                    width: MediaQuery.of(context).size.width * 0.8,
                    child: Text(
                      _author,
                      style: TextStyle(color: Colors.white, fontStyle: FontStyle.italic),
                    ),
                  ),
                  Container(
                    padding:
                        EdgeInsets.only(right: 16.0, left: 5.0, bottom: 10.0),
                    width: MediaQuery.of(context).size.width * 0.8,
                    child: Text(
                      _type,
                      style: TextStyle(color: Colors.white, fontStyle: FontStyle.italic),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
