import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:youth_awakening_society/DrawerElements.dart';
import 'eventCard.dart';
import 'newsCard.dart';
import 'styles.dart';

class Home extends StatelessWidget {
  Home({Key key}) : super(key: key);
  final String title = 'Youth Awakening Society';

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: bgBlue,
          title: Text('Youth Awakening Society'),
        ),
        body: _buildBody(),
        drawer: Drawer(
          child: Container(
            color: bgBlue,
            child: drawerElement(context),
          ),
        ),
      ),
      onWillPop: () {
        return showDialog(
              context: context,
              builder: (context) => new AlertDialog(
                    title: new Text('Are you sure?'),
                    content: new Text('Do you want to exit the App?'),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () => Navigator.of(context).pop(false),
                        child: new Text('No'),
                      ),
                      new FlatButton(
                        /**will not work on iOS because Apple's human interface guidelines state that applications should not exit themselves.**/
                        onPressed: () => SystemNavigator.pop(),
                        child: new Text('Yes'),
                      ),
                    ],
                  ),
            ) ??
            false;
      },
    );
  }

  Widget _buildBody() {
    Firestore firestore = Firestore.instance;
    firestore.settings(timestampsInSnapshotsEnabled: true);

    return SingleChildScrollView(
        child: Container(
      decoration: BoxDecoration(color: bgButter),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            divider('Upcoming Event'),
            event(firestore),
            divider('Message from the president'),
            presidentMessage(firestore),
            divider('News'),
            news(firestore),
          ]),
    ));
  }

  Widget event(Firestore firestore) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          StreamBuilder(
            stream: firestore
                .collection('events')
                .where("timestamp", isGreaterThanOrEqualTo: new DateTime.now())
                .snapshots(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.active) {
                if (snapshot.data.documents.length > 0) {
                  return ListView.builder(
                      shrinkWrap: true,
                      physics: ClampingScrollPhysics(),
                      itemCount: 1,
                      itemBuilder: (context, index) {
                        return _buildListItem(
                            context, snapshot.data.documents[index]);
                      });
                } else {
                  return Center(
                      child: Text(
                    "No Upcoming events",
                    style: TextStyle(fontSize: 12),
                  ));
                }
              } else {
                return Text('Loading....');
              }
            },
          ),
        ]);
  }

  Widget divider(title) {
    return Padding(
      padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 3.0),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              title,
              style: labelStyle,
            ),
            Padding(padding: EdgeInsets.only(bottom: 5.0)),
            Divider(
              height: 0.0,
              color: Colors.black54,
            ),
            Padding(padding: EdgeInsets.only(bottom: 5.0)),
          ]),
    );
  }

  Widget presidentMessage(Firestore firestore) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Flexible(
            child: StreamBuilder(
              stream: firestore.collection('presidentMessage').snapshots(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.active) {
                  return ListView.builder(
                    physics: ClampingScrollPhysics(),
                    padding: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 20.0),
                    shrinkWrap: true,
                    itemCount: 1,
                    itemBuilder: (context, index) {
                      return Text(
                        snapshot.data.documents[index]['message']
                            .toString()
                            .replaceAll("\\n", "\n"),
                        style: TextStyle(fontSize: 15.0),
                      );
                    },
                  );
                } else {
                  return Text('Loading....');
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget news(Firestore firestore) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          StreamBuilder(
            stream: firestore.collection('news').snapshots(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.active) {
                if (snapshot.data.documents.length > 0) {
                  return ListView.builder(
                    physics: ClampingScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: snapshot.data.documents.length,
                    itemBuilder: (context, index) {
                      return _buildNewsList(
                          context, snapshot.data.documents[index]);
                    },
                  );
                } else {
                  return Center(
                    child: Text("No News available"),
                  );
                }
              } else {
                return Text('Loading....');
              }
            },
          ),
        ]);
  }

  Widget _buildListItem(BuildContext context, DocumentSnapshot document) {
    String _name = document['name'];
    String _cardImage = document['image'];
    String _description = document['description'];
    var _timestamp = document['timestamp'];

    return ListTile(
        title: EventCard(_name, _cardImage, _description, _timestamp));
  }

  Widget _buildNewsList(BuildContext context, DocumentSnapshot document) {
    var _title = document['title'];
    var _author = document['author'];
    var _type = document['type'];
    var _article = document['article'];
    var _image = document['image'];

    return ListTile(
      title: NewsCard(_title, _image, _author, _type, _article),
    );
  }
}
