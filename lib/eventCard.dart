import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:date_format/date_format.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'styles.dart';
import 'guibuilder.dart';

class EventCard extends StatefulWidget {
  String _name;
  String _cardImage;
  String _description;
  var _timestamp;

  EventCard(this._name, this._cardImage, this._description, this._timestamp);

  @override
  _EventCard createState() =>
      _EventCard(_name, _cardImage, _description, _timestamp.toDate());
}

class _EventCard extends State {
  String _name;
  String _cardImage;
  String _initial;
  var _timestamp;

  String _description = "Tap to see details";
  bool value = false;

  Icon _dropDown = arrowDown;

  _EventCard(
      String name, String cardImage, String description, var _timestamp) {
    this._name = name;
    this._cardImage = cardImage;
    _initial = description.replaceAll("\\n", "\n");
    _timestamp == null
        ? this._timestamp = Timestamp.now()
        : this._timestamp = _timestamp;
  }

  void _toggle() {
    setState(() {
      if (value) {
        _description = "Tap to see details";
        _dropDown = arrowDown;
      } else {
        _description = _initial;
        _dropDown = arrowUp;
      }
    });
    value = !value;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Container(
        decoration: BoxDecoration(
            color: bgBlue,
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            border: Border.all(
              color: bgBlue,
              width: 1.0,
            )
        ),
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.all(0.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: ClipRRect(
                    borderRadius: BorderRadius.vertical(top:Radius.circular(10.0),bottom: Radius.circular(0.0)),
                    child: CachedNetworkImage(
                      imageUrl: _cardImage,
                      fit: BoxFit.cover,
                      height: 200.0,
                      placeholder: Container(
                        height: 70.0,
                        child: LinearProgressIndicator(),
                      ),
                      errorWidget: Icon(Icons.error),
                      //height: 300,
                    ),
                  ),
                ),
                Positioned(
                  bottom: 0.0,
                  left: 0.0,
                  child: Container(
                    padding: EdgeInsets.only(top: 8.0, left: 5.0, bottom: 8.0),
                    width: MediaQuery.of(context).size.width,
                    color: Color.fromARGB(170, 0, 0, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.9,
                          padding: EdgeInsets.only(top: 0.0),
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.calendar_today,
                                color: Colors.white,
                              ),
                              Expanded(
                                flex: 3,
                                child: Container(
                                  padding: EdgeInsets.only(left: 10.0),
                                  child: Text(
                                    formatDate(
                                        new DateTime(
                                            _timestamp.year,
                                            _timestamp.month,
                                            _timestamp.day,
                                            _timestamp.hour,
                                            _timestamp.minute,
                                            _timestamp.second),
                                        [d, ' ', 'M', ' ', yyyy]),
                                    style: TextStyle(
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                                ),
                              ),
                              Icon(
                                Icons.schedule,
                                color: Colors.white,
                              ),
                              Expanded(
                                flex: 2,
                                child: Container(
                                  padding: EdgeInsets.only(left: 10.0),
                                  child: Text(
                                    formatDate(
                                        new DateTime(
                                            _timestamp.year,
                                            _timestamp.month,
                                            _timestamp.day,
                                            _timestamp.hour,
                                            _timestamp.minute,
                                            _timestamp.second),
                                        [hh, ':', nn, ' ', am]),
                                    style: TextStyle(
                                        fontSize: 15.0,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                _toggle();
              },
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.only(left: 5.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          buildText(_name, cardTitle, 10.0),
                          Container(
                            padding: EdgeInsets.all(10.0),
                            child: Text(
                              _description,
                              maxLines: 1000,
                              overflow: TextOverflow.ellipsis,
                              softWrap: false,
                              style: cardText,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    height: 40.0,
                    padding: EdgeInsets.only(left: 20.0, right: 20.0),
                    child: _dropDown,
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
