import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:youth_awakening_society/content.dart';
import 'package:youth_awakening_society/guibuilder.dart';
import 'package:youth_awakening_society/styles.dart';

class AboutState extends State {
  final String title = 'About Us';
  AboutContent content;
  Widget _childContent = Center(child: Text('Loading...'));

  @override
  Widget build(BuildContext context) {
    loadContent();
    return buildScreen(context, _childContent);
  }

  Future loadContent() async {
    String jsonContent = await loadAsset('assets/data/about.json');
    Map<String, dynamic> decoded = await jsonDecode(jsonContent);
    setState(() {
      content = AboutContent(
        decoded['info']['title'], decoded['info']['desc'],
        decoded['area']['title'], decoded['area']['desc'],
        decoded['working']['title'], decoded['working']['desc'],
        decoded['mission']['title'], decoded['mission']['desc'],
        decoded['vision']['title'], decoded['vision']['desc'],
        decoded['obj']['title'], decoded['obj']['objs'],
      );
      _childContent = buildContent(content);     
    });
  }

  Container buildContent(AboutContent content) {
    var objectives = <Widget>[];
    for (String obj in content.objs) {
      objectives.add(buildBulletText(obj));
    }
    final objs = Column(
      children: objectives,
    );
    return buildSliverList(title, 'assets/images/aboutus.jpg', <Widget>[
      buildText(content.name, titleStyle),
      buildText(content.info, defaultStyle),
      buildText(content.missionTitle, titleStyle),
      buildText(content.missionDesc, defaultStyle),
      buildText(content.visionTitle, titleStyle),
      buildText(content.visionDesc, defaultStyle),
      buildText(content.objTitle, titleStyle),
      objs,
      buildText(content.areaTitle, titleStyle),
      buildText(content.areaDesc, defaultStyle),
      buildText(content.workTitle, titleStyle),
      buildText(content.workDesc, defaultStyle),
    ]);
  }
}

class About extends StatefulWidget {
  @override
  AboutState createState() => AboutState();
}