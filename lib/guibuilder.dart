import 'package:flutter/material.dart';
import 'package:youth_awakening_society/styles.dart';
import 'package:youth_awakening_society/DrawerElements.dart';

Container buildText(String text, TextStyle style, [double padding]) {
  var value;
  if (padding == null) {
    value = EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0);
  } else {
    value = EdgeInsets.fromLTRB(padding, padding, padding, 0.0);
  }
  return Container(
      alignment: Alignment(-1.0, -1.0),
      padding: value,
      child: Text(text, style: style));
}

Container buildBulletText(String text) {
  return Container(
      alignment: Alignment(-1.0, -1.0),
      padding: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 20.0),
      child: Text('‣  ' + text, style: defaultStyle));
}

WillPopScope buildScreen(BuildContext context, Widget childContent) {
  return WillPopScope(
    child: Scaffold(
      body: childContent,
      drawer: Drawer(
        child: Container(
          color: bgBlue,
          child: drawerElement(context),
        ),
      ),
    ),
    onWillPop: () {
      Navigator.popAndPushNamed(context, '/home');
    },
  );
}

SliverAppBar sliverAppBar(
    {String articletitle, @required String imgPath, @required String title}) {
  return SliverAppBar(
    expandedHeight: 150.0,
    floating: false,
    pinned: true,
    flexibleSpace: LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        double top = constraints.biggest.height;
        return FlexibleSpaceBar(
          title: AnimatedOpacity(
            duration: Duration(milliseconds: 300),
            opacity: top < 150.0 ? 1.0 : 0.0,
            child: Text(
              title,
              style: TextStyle(color: Colors.white),
              overflow: TextOverflow.ellipsis,
            ),
          ),
          background: (articletitle == 'News')
              ? FadeInImage.assetNetwork(
                  fit: BoxFit.cover,
                  placeholder: 'assets/images/blog.jpg',
                  image: imgPath)
              : Image.asset(
                  imgPath,
                  fit: BoxFit.fitWidth,
                ),
        );
      },
    ),
  );
}

Container buildSliverList(String title, String imgPath, List<Widget> list) {
  return Container(
    color: bgButter,
    child: CustomScrollView(
      slivers: <Widget>[
        sliverAppBar(title: title, imgPath: imgPath),
        SliverList(delegate: SliverChildListDelegate(list)),
      ],
    ),
  );
}

Container buildSliverListWithGrid(
    String title, String imgPath, List<Widget> list, List<Widget> grid) {
  return Container(
    color: bgButter,
    child: CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          expandedHeight: 150.0,
          floating: false,
          pinned: true,
          flexibleSpace: FlexibleSpaceBar(
            title: Text(
              title,
              style: TextStyle(color: Colors.white),
            ),
            background: Image.asset(
              imgPath,
              fit: BoxFit.cover,
            ),
          ),
        ),
        SliverList(delegate: SliverChildListDelegate(list)),
        SliverGrid(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: 1.0,
          ),
          delegate: SliverChildListDelegate(grid),
        )
      ],
    ),
  );
}

Widget buildProgramCard(
    BuildContext context, String title, String desc, ImageProvider image) {
  return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Container(
        decoration: BoxDecoration(
          color: bgButter,
          borderRadius: BorderRadius.circular(10.0),
          border: Border.all(
            color: bgBlue.withOpacity(0.3),
            width: 1.0,
          )
        ),
              child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0), topRight: Radius.circular(10.0)),
                  child: Image(
                    image: image,
                    fit: BoxFit.cover,
                    width: MediaQuery.of(context).size.width,
                    height: 300,
                  ),
                ),
                Positioned(
                    bottom: 0.0,
                    left: 0.0,
                    child: Container(
                        color: Color.fromARGB(100, 0, 0, 0),
                        width: MediaQuery.of(context).size.width - 20.0,
                        child: buildText(title, whiteTitleStyle)))
              ],
            ),
            buildText(desc, defaultStyle)
          ],
        ),
      ));
}
