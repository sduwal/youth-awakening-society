import 'package:flutter/material.dart';
import 'blogView.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'styles.dart';

class NewsCard extends StatelessWidget {
  String _title;
  String _author;
  String _type;
  String _article;
  String _image;

  NewsCard(this._title, this._image, this._author, this._type, this._article);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: bgBlue,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(topLeft: Radius.circular(100.0), topRight: Radius.circular(10.0), bottomRight: Radius.circular(10.0), bottomLeft: Radius.circular(100.0)),
        // borderRadius: BorderRadius.circular(15.0),
      ),
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => BlogView('News', _image, _title, _author,
                    _article.replaceAll("\\n", "\n\n"))),
          );
        },
        child: Row(children: <Widget>[
          Container(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ClipOval(
                    child: Container(
                      child: CachedNetworkImage(
                        imageUrl: _image,
                        width: 100.0,
                        height: 100.0,
                        fit: BoxFit.cover,
                        placeholder: Image.asset(
                          'assets/images/Logo.jpg',
                          height: 100.0,
                          width: 100.0,
                        ),
                      ),
                    ),
                  ),
                ),
                Flexible(
                  fit: FlexFit.loose,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.fromLTRB(5.0, 5.0, 16.0, 5.0),
                        width: MediaQuery.of(context).size.width * 0.85 - 100.0,
                        child: Text(
                          _title,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold, fontSize: 25.0,
                          ),
                        ),
                      ),
                      Container(
                        padding:
                            EdgeInsets.only(top: 16.0, right: 16.0, left: 5.0),
                        width: MediaQuery.of(context).size.width * 0.85 - 100.0,
                        child: Text(
                          _author,
                          style: TextStyle(color: Colors.white,fontStyle: FontStyle.italic),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            right: 16.0, left: 5.0, bottom: 10.0),
                        width: MediaQuery.of(context).size.width * 0.85 - 100.0,
                        child: Text(
                          _type,
                          style: TextStyle(color: Colors.white,fontStyle: FontStyle.italic),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ]),
      ),
    );
  }
}
