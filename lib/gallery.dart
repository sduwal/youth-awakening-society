import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'imageViewer.dart';
import 'guibuilder.dart';
import 'styles.dart';

class Gallery extends StatefulWidget {
  _Gallery createState() => _Gallery();
}

class _Gallery extends State {
  final String title = 'Gallery';

  @override
  Widget build(BuildContext context) {
    return buildScreen(context, _buildBody());
  }

  Widget _buildBody() {
    Firestore firestore = Firestore.instance;
    firestore.settings(timestampsInSnapshotsEnabled: true);

    return Container(
      color: bgButter,
      child: CustomScrollView(
        slivers: <Widget>[
          sliverAppBar(title: title, imgPath: 'assets/images/gallery.jpg'),
          StreamBuilder(
            stream: firestore.collection('gallery').snapshots(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.active) {
                return SliverGrid(
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: MediaQuery.of(context).size.width / 3,
                    mainAxisSpacing: 1.0,
                    crossAxisSpacing: 1.0,
                  ),
                  delegate: SliverChildBuilderDelegate((context, index) {
                    return _buildGridTiles(
                        context,
                        snapshot.data.documents[
                            snapshot.data.documents.length - index - 1]);
                  }, childCount: snapshot.data.documents.length),
                );
              } else {
                return SliverList(
                    delegate: SliverChildBuilderDelegate(
                  (context, index) {
                    return ListTile(
                      title: Center(
                          child: Text(
                        "Loading....",
                        style: TextStyle(
                          fontSize: 30.0,
                          fontStyle: FontStyle.italic,
                        ),
                      )),
                    );
                  },
                  childCount: 1,
                ));
              }
            },
          ),
        ],
      ),
    );
  }

  Widget _buildGridTiles(BuildContext context, DocumentSnapshot documents) {
    return Container(
        child: GestureDetector(
            onTap: () {
              print('Tapped');
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ImageViewer(
                        documents['image'], documents['description'])),
              );
            },
            child: FadeInImage.assetNetwork(
              placeholder: 'assets/images/loading.gif',
              image: documents['image'],
            )));
  }
}
