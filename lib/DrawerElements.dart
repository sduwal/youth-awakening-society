import 'package:flutter/material.dart';
import 'styles.dart';

Widget drawerElement(BuildContext context) {
  List<String> titles = [
    "Header",
    "Home",
    "Events",
    "Gallery",
    "Blog",
    "-",
    "Services",
    "About Us",
    "Contact"
  ];

  List icons = [
    Icons.home,
    Icons.event,
    Icons.collections,
    Icons.book,
    "-",
    Icons.work,
    Icons.person_outline,
    Icons.contact_phone
  ];

  final double statusBarHeight = MediaQuery.of(context).padding.top;

  return ListView.builder(
      padding: EdgeInsets.only(top: statusBarHeight),
      itemCount: titles.length,
      itemBuilder: (context, position) {
        if (position == 0) {
          return DrawerHeader(
            decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/logo_original.png'),
                  fit: BoxFit.fitHeight,
                ),
                color: bgBlue),
          );
        }

       // if (position < titles.length) {
          if( titles[position]=="-"){
            return Container(
              child: Divider(height: 5.0,color: Colors.white,),

            );
          }
          return ListTile(
            title: Row(
              children: <Widget>[
                Container(
                    child: Icon(
                      icons[position - 1],
                      color: Colors.white,
                    )),
                Expanded(
                    child: Container(
                      padding: EdgeInsets.only(left: 12.0),
                      child: Text(
                        titles[position],
                        style: TextStyle(color: Colors.white),
                      ),
                    ))
              ],
            ),
            onTap: () => onTapped(context, titles[position]),
          );
       // }
      });
}

Widget onTapped(BuildContext context, String title) {
  Navigator.of(context).pop();
  var route=title.toLowerCase();
  Navigator.pushNamed(context, '/$route');
//    Scaffold.of(context).removeCurrentSnackBar();
//    final snackBar = SnackBar(
//      content: Text(
//        title,
//        style: TextStyle(color: Colors.white),
//      ),
//    );
//    Scaffold.of(context).showSnackBar(snackBar);
  return null;
}