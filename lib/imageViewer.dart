import 'package:flutter/material.dart';

class ImageViewer extends StatelessWidget {
  String _name;
  String _description;
  ImageViewer(String _name, String _description) {
    this._name = _name;
    this._description = _description.replaceAll("\\n", "\n");
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            centerTitle: true,
            expandedHeight: MediaQuery.of(context).size.height,
            floating: false,
            pinned: true,
            flexibleSpace: FlexibleSpaceBar(
              title: Container(
                padding: EdgeInsets.all(10.0),
                color: Color.fromARGB(155, 0, 0, 0),
                width: MediaQuery.of(context).size.width*0.7,
                child: Text(
                 _description,
                  style: TextStyle(color: Colors.white,fontSize: 15.0,fontStyle: FontStyle.normal,fontWeight: FontWeight.w100),
                ),
              ),
              background: Image.network(
                _name,
                fit: BoxFit.fitWidth,
              ),
            ),
          )
        ],
      )
    );
  }
}
