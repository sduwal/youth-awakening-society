import 'dart:async' show Future;
import 'package:flutter/services.dart' show rootBundle;

Future<String> loadAsset(String path) async {
  return await rootBundle.loadString(path);
}

class ContactContent {
  final String address;
  final String email;
  final String phone;
  var hours;
  ContactContent(this.address, this.email, this.phone, this.hours);
}

class Service {
  final String title;
  final String desc;
  final String img;

  Service(this.title, this.desc, this.img);
}

class ServicesContent {
  final String title;
  final String desc;
  final List<Service> progs;

  ServicesContent(
    this.title, this.desc,
    this.progs
  );
}

class AboutContent {
  final String name;
  final String info;
  final String areaTitle;
  final String areaDesc;
  final String workTitle;
  final String workDesc;
  final String missionTitle;
  final String missionDesc;
  final String visionTitle;
  final String visionDesc;
  final String objTitle;
  var objs;

  AboutContent(
    this.name, this.info,
    this.areaTitle, this.areaDesc,
    this.workTitle, this.workDesc,
    this.missionTitle, this.missionDesc,
    this.visionTitle, this.visionDesc,
    this.objTitle, this.objs
  );
}